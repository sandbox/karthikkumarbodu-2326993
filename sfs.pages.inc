<?php

/**
 * @file
 * Administrative pages for listing secure file's shared.
 */

/**
 * Function to generate a table with the list of.
 *
 * Files shared by the logged in user.
 */
function sfs_list_files_sharedby() {
  global $user;

  $header = array(t('File Name'),
    t('File Size(MB)'),
    t('Shared To'),
    t('Valid Till'),
    t('Status'),
  );

  $query = db_select('sfs_files', 'sf')->extend('PagerDefault');
  $query->join('file_managed', 'fm', 'fm.fid = sf.sfs_id');

  $query->condition('uid', $user->uid, '=');
  $query->fields('fm', array('fid', 'filename', 'filesize'));
  $query->fields('sf', array('internal_users', 'expiration', 'status'));
  $query->orderBy('timestamp', 'DESC');
  $query->limit(10);
  $results = $query->execute();
  $rows = array();
  foreach ($results as $row) {
    // Retrieving user names.
    $user_ids = unserialize($row->internal_users);
    foreach ($user_ids as $user_id) {
      $users[$user_id] = db_query("SELECT name FROM {users} WHERE uid = :uid", array(':uid' => $user_id))->fetchField();
    }
    $user_names = implode(', ', $users);
    // Retrieving file download expiration.
    if (empty($row->expiration)) {
      $expiration = 'N/A';
    }
    else {
      $expiration = date('m/d/Y H:i:s', $row->expiration);
    }
    // Retrieving file status.
    $status = ($row->status == 1 ? 'Active' : 'Deactive');
    $rows[] = array($row->filename,
      round(($row->filesize) / (1024 * 1024), 2),
      $user_names,
      $expiration,
      l($status, 'sfs/toggle/status'),
    );
  }
  $output = theme('table', array('header' => $header, 'rows' => $rows));
  $output .= theme('pager');

  return $output;
}

/**
 * Function to generate a table with the list of.
 *
 * Files shared with the logged in user.
 */
function sfs_list_files_sharedto() {
  global $user;

  $header = array(t('File Name'),
    t('File Size(MB)'),
    t('Shared To'),
    t('Valid Till'),
    t('Status'),
  );

  $query = db_select('sfs_files', 'sf')->extend('PagerDefault');
  $query->join('file_managed', 'fm', 'fm.fid = sf.sfs_id');
  $query->condition('sf.internal_users', '%' . $user->uid . '%', 'LIKE');
  $query->fields('fm', array('filename', 'filesize'));
  $query->fields('sf', array('internal_users', 'expiration', 'status'));
  $query->orderBy('timestamp', 'DESC');
  $query->limit(10);

  $results = $query->execute();

  // Build the table fields.
  $rows = array();
  foreach ($results as $row) {
    // Retrieving user names.
    $user_ids = unserialize($row->internal_users);

    foreach ($user_ids as $user_id) {
      $users[$user_id] = db_query("SELECT name FROM {users} WHERE uid = :uid", array(':uid' => $user_id))->fetchField();
    }
    $user_names = implode(', ', $users);
    // Retrieving file download expiration.
    if (empty($row->expiration)) {
      $expiration = 'N/A';
    }
    else {
      $expiration = date('m/d/Y H:i:s', $row->expiration);
    }
    // Retrieving file status.
    $status = ($row->status == 1 ? 'Active' : 'Deactive');

    $rows[] = array($row->filename,
      round(($row->filesize) / (1024 * 1024), 2),
      $user_names,
      $expiration,
      l($status, 'sfs/toggle/status'),
    );
  }
  $output = theme('table', array('header' => $header, 'rows' => $rows));

  $output .= theme('pager');

  return $output;
}
