<?php

/**
 * @file
 * Administrative pages for secure file share.
 */

/**
 * Administrative page to configure secure file share settings.
 */
function sfs_settings_form($form, &$form_state) {
  $form = array();
  $form['sfs_size'] = array(
    '#title' => t('File Size'),
    '#type' => 'textfield',
    '#size' => 20,
    '#required' => TRUE,
    '#default_value' => variable_get('sfs_size', ''),
    '#description' => t("Choose a size limit for the upload file's in <b>MB</b>."),
  );
  $form['sfs_types'] = array(
    '#title' => t("File Type's"),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('sfs_types', ''),
    '#description' => t("Choose allowed file extensions for the upload file's as comma separated values."),
  );
  $form['sfs_password_protection'] = array(
    '#title' => t('Password Protection'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('sfs_password_protection', ''),
    '#description' => t("Check this box if you would like to enable password protection for the upload file's."),
  );
  $form['sfs_expiration'] = array(
    '#title' => t('Expiration'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('sfs_expiration', ''),
    '#description' => t("Check this box if you would like to set expiration time for the upload file's."),
  );

  $form['email_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t("Email Setting's"),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['email_settings']['sfs_mail_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#required' => TRUE,
    '#default_value' => variable_get('sfs_mail_subject', ''),
  );
  $form['email_settings']['sfs_mail_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#required' => TRUE,
    '#rows' => 10,
    '#default_value' => variable_get('sfs_mail_body', ''),
  );
  $form['email_settings']['replacement_help'] = array(
    '#type' => 'fieldset',
    '#title' => t('Available tokens for replacement'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 1,
  );
  $form['email_settings']['replacement_help']['sfs_replacements'] = array(
    '#markup' => _sfs_get_replacement_patterns(),
  );
  return system_settings_form($form);
}

/**
 * Function to create table with the list of replacement patterns.
 *
 * Available for using them in email.
 */
function _sfs_get_replacement_patterns() {
  $header = array('Name', 'Replacement Pattern', 'Description');

  $rows[] = array(t('File Shared By'),
    '[sfs:file-shared-by]',
    t('This contains the name of the user, the file is shared by.'),
  );
  $rows[] = array(t('File Shared On'),
    '[sfs:file-shared-on]',
    t("This contains the date on which the file is shared with user's."),
  );
  $rows[] = array(t('File URL'),
    '[sfs:file-url]',
    t('This contains the url to download the file.'),
  );
  $rows[] = array(t('File Password'),
    '[sfs:file-password]',
    t('This contains the password to download the file.'),
  );
  return theme('table', array('header' => $header, 'rows' => $rows));
}
